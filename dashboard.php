<?php require_once('head.php');?>
  <body>
    <header>
        <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
        <?php include('menu.php');?>
    </header>
    <div>
        <h1>Všechny články:</h1>
        <?php
        $stmt = $conn->prepare("SELECT clanky.idclanky, clanky.titulek, clanky.obsah, autor.jmeno as jmenoautor, autor.prijmeni FROM clanky JOIN autor ON autor.idautor = clanky.idautor");
        $stmt->execute();
        
        while ($row = $stmt->fetch()) {
            echo '<h2>'.$row['titulek'].'</h2>';
            echo '<h3>'.$row['jmenoautor'].' '.$row['prijmeni'].'</h3>';
            echo '<p>'.$row['obsah'].'</p>';
            echo '<br>';
        }
        ?>
    </div>