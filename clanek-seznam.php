<?php require_once('head.php');?>
<?php require_once('fce/check_permission.php');?>

<body>
<header>
    <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
    <?php include('menu.php');?>
</header>

<div class="content">

    <?php
        $stmt = $conn->prepare("SELECT idclanky, titulek FROM clanky ORDER BY idclanky ASC;");
        $stmt->execute();

        while ($row = $stmt->fetch()) {
            echo '<p></p><a href="clanek-edit.php?idclanek='.$row['idclanky'].'">'.$row['titulek'].'</a></p>';
        }
    ?>

</div>
<?php include('footer.php');?>

</body>
</html>