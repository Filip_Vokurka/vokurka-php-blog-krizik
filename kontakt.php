<?php require_once('head.php');?>
  <body>
    <header>
        <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
        <?php include('menu.php');?>
    </header>
    <div>
        <article class="kntakty">
            <h2>Kontakty</h2>
            <p>tel. 224 210 585</p>
            <p>Email: info@skolakrizik.cz</p>
            <p>Sídlo: Na Příkopě 856/16, 11000 Praha 1 – Nové Město</p>
            <p>IČ: 70837881</p>
            <p>DIČ: CZ70837881</p>
            <h3>Úřední hodiny:</h3>
            <p>pondělí – čtvrtek 8:00 – 15:00</p>
            <p>pátek 8:00 – 14:30</p>
        </article>
    </div>