<?php require_once('head.php');?>
<body>
    <header>
        <img class="logo" src="logo.png" alt="logo CoolBlog" width="200" height="110" />
        <?php include('menu.php');?>
    </header>
    <div>
        <h1>Články</h1>
        <?php
            $stmt = $conn->prepare("SELECT clanky.titulek, clanky.obsah, clanky.idclanky, autor.jmeno as jmenoautor, autor.prijmeni FROM clanky JOIN autor ON autor.idautor = clanky.idautor ORDER BY  clanky.idclanky");
            $stmt->execute();
            
            while ($row= $stmt->fetch()){
                echo '<h2>'.$row['titulek'].'</h2>';
                if(isset($_GET['id'])&& $_GET['id']==$row['idclanky']){
                    echo '<p>'.$row['obsah'].'</p>';
                    echo '<p class= "autor">'.$row['jmenoautor'].' '.$row['prijmeni'].'</p>';
                }
                echo '<p class="link"><a href="clanky.php?id='.$row['idclanky'].'">Přečíst</a></p>';
                echo '</div>';
            }
        ?>
    </div>
        <?php include('footer.php');?>
</body>
</hmtl>